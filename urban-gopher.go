package main

import (
	builtinLog "log"
	"net/http"
	"os"

	"gitlab.com/dgarlitt/urban-gopher/pkg/config"
	customLog "gitlab.com/dgarlitt/urban-gopher/pkg/log"
	"gitlab.com/dgarlitt/urban-gopher/pkg/routing"
)

func main() {
	config.APIKey = os.Getenv("URBAN_GOPHER_API_KEY")
	customLog.SetOutput()
	routing.SetupRoutes()
	builtinLog.Fatal(http.ListenAndServe(":8008", nil))
}
