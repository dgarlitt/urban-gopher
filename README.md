[![build status](https://gitlab.com/dgarlitt/urban-gopher/badges/master/build.svg)](https://gitlab.com/dgarlitt/urban-gopher/commits/master)
[![codecov](https://codecov.io/gl/dgarlitt/urban-gopher/branch/master/graph/badge.svg)](https://codecov.io/gl/dgarlitt/urban-gopher)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/dgarlitt/urban-gopher)](https://goreportcard.com/report/gitlab.com/dgarlitt/urban-gopher)

![Urban Gopher Logo](https://raw.githubusercontent.com/dgarlitt/image-repo/master/urban-gopher/urban-gopher-art.jpg)

# Urban Dictionary Definition Lookup Service

This is a simple service that will look-up the "best" definition for a given term
in Urban Dictionary using the Mashape API. You will need to provide a Mashape
API Key to make a request from this service.

A Mashape API key can be provided as an environment variable:

```
export URBAN_GOPHER_API_KEY=<your-mashape-api-key>
```

Then you can startup the service and hit the endpoint as follows:

```
curl http://localhost:8008/definition?term=wat
```

Alternatively, you can provide the API key as a header when making a request:

Example request:

```
curl -H "X-API-Key: <your-mashape-api-key>" http://localhost:8008/definition?term=wat
```

Mashape API keys will not be logged in the log output.

## Install and Run

```sh
go get gitlab.com/dgarlitt/urban-gopher
cd $GOPATH/src/gitlab.com/dgarlitt/urban-gopher
go build -v -race -o deploy/artifacts/urban-gopher
deploy/artifacts/urban-gopher
```


# Encrypt Gitlab Variables

You can create encrypted variable values using the following method.

 - Generate a key to use for encryption
  - `openssl genrsa -out encryption.key 2048`
 - Go to `Settings -> Variables`
   - Under "Add A Variable"
   - Type `DECRYPTOR` for the variable name
   - For the value, put the result of this command:
    - `cat encryption.key | base64`
    - The use of `base64` for this step is not necessary, it just adds a layer
      of obfuscation.
   - Click "Add New Variable"
 - Create another variable named `MY_ENCRYPTED_VAR`
 - For the value, use the output of this command:
  - `echo 'SuperSecretPassword' | openssl rsautl -inkey encryption.key -encrypt | base64`
 - Click "Add New Variable"

`WARNING`: Make sure not to commit the encryption.key file to your repository.

Now you can access these encrypted variable values in your build using the following:

```
echo $DECRYPTOR | base64 -d > encryption.key
MY_VAR=$(echo $MY_ENCRYPTED_VAR | base64 -d | openssl rsautl -inkey encryption.key -decrypt)
```

Note: If you did not use `base64` for the value of the DECRYPTOR variable at the
beginning, you shouldn't use it to create the encryption.key file during your
build.
